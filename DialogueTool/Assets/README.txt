
HOW TO WRITE DIALOGUE IN TEXT FILE:

	- go to Tools->Create Dialogue
	- you will need to enter the correct name of a text file that is located in Assets/DialogueTool/Resources
	- choose where to save
	- parser will automatically parse data from text file to corresponding data in created scriptable object
	- place created scriptable object in Dialogue Manager script and play (space to go to the next sentence)
	- *Name*: it is getting the character scriptable object by the name (in the Resources/Characters there should be object with the corresponding name)

	(be careful when writing to a text file, it is for now case sensitive and all that...)

		EXAMPLE TEXT

*Conversation*:Tutorial	-> Tutorial will be the name of the dialogue
*ID*:001				-> 001 will be set as the ID of the dialogue
{						-> indicates the start of the conversation
	*Node*:				-> indicates a new node
	(
		*Name*:Mark						-> characters name
		*Content*:Hello, how are you?	-> main text / sentence that will be displayed
		*Event*:TalkedToAnt				-> event that would be triggered
	)
	*Node*:
	(
		*Name*:Ant
		*Content*:Awesome, just looking for some food.
	)
}