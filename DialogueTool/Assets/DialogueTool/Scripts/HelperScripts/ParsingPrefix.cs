﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParsingPrefix
{
    public const string Print = "*Print*:";

    public const string Conversation = "*Conversation*:";
    public const string ID = "*ID*:";
    public const string Name = "*Name*:";
    public const string Node = "*Node*:";
    public const string Content = "*Content*:";
    public const string Choice = "*Choice*:";
    public const string Option = "*Option*:";
    public const string Event = "*Event*:";
    public const string Audio = "*Audio*:";
    public const string NodeWhenDone = "*NodeWhenDone*:";

    public const string Dialogues = "*Dialogues*:";
    public const string Dialogue = "*Dialogue*:";

    public const char ConversationBegin = '{';
    public const char ConversationEnd = '}';
    public const char NodeBegin = '(';
    public const char NodeEnd = ')';
}
