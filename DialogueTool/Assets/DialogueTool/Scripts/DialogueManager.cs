﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class DialogueManager : MonoBehaviour
{
    public float speedOfShowingText = 2f;
    public DialogueSO obtainedDialogue;
    public GameObject choiceText;
    private AudioSource audioSource;
    private int currNode = 0;
    public bool isTalking;

    private void Start()
    {
        UIGenerated.AddReferences();
        audioSource = GetComponent<AudioSource>();
        UIGenerated.DialogueCanvas.SetActive(false);
        UIGenerated.DialoguesToGoPanel.SetActive(false);
        isTalking = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !isTalking)
        {
            StartDialogue(obtainedDialogue);
        }
    }

    #region Custom methods

    void StartDialogue(DialogueSO obtainedDialogue)
    {
        isTalking = true;
        UIGenerated.DialogueCanvas.SetActive(true);
        if (obtainedDialogue.currNode < obtainedDialogue.nodes.Count)
        {
            StopAllCoroutines();
            audioSource.Stop();
            var node = obtainedDialogue.nodes[obtainedDialogue.currNode];
            UIGenerated.SentencesText.GetComponent<Text>().text = "";
            UIGenerated.CharacterImage.GetComponent<Image>().sprite = node.character.characterSprite;
            if (node.choices.Count > 0)
            {
                foreach (var choice in node.choices)
                {
                    GameObject tempChoiceText = Instantiate(choiceText, UIGenerated.ChoicePanel.transform);
                    tempChoiceText.GetComponent<Text>().text = choice + "\n";
                }
                StartCoroutine(WaitForChoicePick(node.choices, obtainedDialogue));

            }
            if (node.events.Count > 0)
            {
                for (int i = 0; i < node.events.Count; i++)
                {
                    EventManager.TriggerEvent(node.events[i]);
                }
            }
            if (node.sentence != "")
            {
                StartCoroutine(TextEffect(node.character.characterName.ToUpper() + ": " + node.sentence, obtainedDialogue));
            }
            if (node.audioClip)
            {
                audioSource.clip = node.audioClip;
                audioSource.Play();
            }
            if (node.stringToPrint != "")
            {
                Debug.Log(node.stringToPrint);
            }            
        }
        
        if (obtainedDialogue.currNode >= obtainedDialogue.nodes.Count && !obtainedDialogue.isDialogueShown)
        {
            obtainedDialogue.isDialogueShown = true;
            UIGenerated.SentencesText.GetComponent<Text>().text = "";
            if (obtainedDialogue.dialoguesToGo.Count > 0)
            {
                for (int i = 0; i < UIGenerated.DialoguesToGoPanel.transform.childCount; i++)
                {
                    Destroy(UIGenerated.DialoguesToGoPanel.transform.GetChild(i).gameObject);
                }
                for (int i = 0; i < obtainedDialogue.dialoguesToGo.Count; i++)
                {
                    GameObject dialogueToGoText = Instantiate(choiceText, UIGenerated.DialoguesToGoPanel.transform);
                    if (obtainedDialogue.dialoguesToGo[i])
                    {
                        dialogueToGoText.GetComponent<Text>().text = "Press " + i + " : " + obtainedDialogue.dialoguesToGo[i].dialogueName;
                    }
                }

                StartCoroutine(WaitForDialoguePick(obtainedDialogue.dialoguesToGo));
            }
            isTalking = false;

            UIGenerated.DialogueCanvas.SetActive(false);
        }
        else if (obtainedDialogue.isDialogueShown)
        {
            if (obtainedDialogue.nodesWhenDialogueIsDone.Count > 0)
            {
                DialogueIsDoneText(obtainedDialogue);
            }
            else
            {
                UIGenerated.DialogueCanvas.SetActive(false);
                isTalking = false;
            }
        }      
    }


    void DialogueIsDoneText(DialogueSO dialogue)
    {
        UIGenerated.DialogueCanvas.SetActive(true);
        UIGenerated.CharacterImage.GetComponent<Image>().sprite = dialogue.nodesWhenDialogueIsDone[0].character.characterSprite;
        UIGenerated.SentencesText.GetComponent<Text>().text = "";
        StartCoroutine(TextEffect(dialogue.nodesWhenDialogueIsDone[0].sentence, UIGenerated.SentencesText.GetComponent<Text>()));
    }

    #endregion

    #region Coroutines
    IEnumerator WaitForDialoguePick(List<DialogueSO> dialoguesToGo)
    {
        //TODO: ne radi jer prepoznaje samo prvi waitWhile, dohvacanje keyCode-a pa pregled koji gumb je stisnut takoder nije radilo
        UIGenerated.DialoguesToGoPanel.SetActive(true);

        yield return new WaitWhile(() => !Input.GetKeyDown(KeyCode.Alpha0));

        if (dialoguesToGo[0])
        {
            UIGenerated.DialoguesToGoPanel.SetActive(false);
            obtainedDialogue = dialoguesToGo[0];
            StartDialogue(dialoguesToGo[0]);
        }

        yield return new WaitWhile(() => !Input.GetKeyDown(KeyCode.Alpha1));
        if (dialoguesToGo[1])
        {
            UIGenerated.DialoguesToGoPanel.SetActive(false);
            obtainedDialogue = dialoguesToGo[1];
            StartDialogue(dialoguesToGo[1]);
        }

        yield return new WaitWhile(() => !Input.GetKeyDown(KeyCode.Alpha2));
        if (dialoguesToGo[2])
        {
            UIGenerated.DialoguesToGoPanel.SetActive(false);
            obtainedDialogue = dialoguesToGo[2];
            StartDialogue(dialoguesToGo[2]);
        }
    }
    IEnumerator WaitForChoicePick(List<string> choices, DialogueSO currDialogue)
    {
        //TODO: kako najbolje riješiti da se ovdje actually moze pritisnuti text i izabrati nesto? (event trigger na svaki tekst posebno pa se dohvati text i procita sadržaj pa to posalje?)
        UIGenerated.ContinueText.SetActive(true);
        yield return new WaitWhile(() => !Input.GetMouseButtonDown(0));

        //brise ih
        for (int i = 0; i < UIGenerated.ChoicePanel.transform.childCount; i++)
        {
            Destroy(UIGenerated.ChoicePanel.transform.GetChild(i).gameObject);
        }

        UIGenerated.ContinueText.SetActive(false);
        currDialogue.currNode++;

        StartDialogue(currDialogue);
    }

    IEnumerator TextEffect(string text, DialogueSO currDialogue)
    {
        foreach (var character in text.ToCharArray())
        {
            UIGenerated.SentencesText.GetComponent<Text>().text += character;
            yield return new WaitForSeconds(Time.fixedDeltaTime * speedOfShowingText);
        }
        StartCoroutine(WaitForContinuePress(currDialogue));
    }
    IEnumerator TextEffect(string text, Text textComponent)
    {
        foreach (var character in text.ToCharArray())
        {
            textComponent.text += character;
            yield return new WaitForSeconds(Time.fixedDeltaTime * speedOfShowingText);
        }

        UIGenerated.ContinueText.SetActive(true);
        yield return new WaitWhile(() => !Input.GetKeyDown(KeyCode.Space));
        UIGenerated.SentencesText.GetComponent<Text>().text = "";
        UIGenerated.ContinueText.SetActive(false);
        UIGenerated.DialogueCanvas.SetActive(false);
        isTalking = false;
    }

    IEnumerator WaitForContinuePress(DialogueSO currDialogue)
    {
        UIGenerated.ContinueText.SetActive(true);
        yield return new WaitWhile(() => !Input.GetKeyDown(KeyCode.Space));
        audioSource.Stop();
        UIGenerated.ContinueText.SetActive(false);
        currDialogue.currNode++;
        StartDialogue(currDialogue);
    }

    #endregion


}
