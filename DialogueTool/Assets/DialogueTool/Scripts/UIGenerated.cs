using UnityEngine;
public class UIGenerated : MonoBehaviour
{
	public static GameObject DialogueCanvas;
	public static GameObject SentencesPanel;
	public static GameObject CharacterImage;
	public static GameObject SentencesText;
	public static GameObject ContinueText;
	public static GameObject ChoicePanel;
	public static GameObject EventText;
	public static GameObject DialoguesToGoPanel;

	public static void AddReferences()
	{
		DialogueCanvas = GameObject.Find("DialogueCanvas");
		SentencesPanel = GameObject.Find("DialogueCanvas/SentencesPanel");
		CharacterImage = GameObject.Find("DialogueCanvas/SentencesPanel/CharacterImage");
		SentencesText = GameObject.Find("DialogueCanvas/SentencesPanel/SentencesText");
		ContinueText = GameObject.Find("DialogueCanvas/SentencesPanel/ContinueText");
		ChoicePanel = GameObject.Find("DialogueCanvas/ChoicePanel");
		EventText = GameObject.Find("DialogueCanvas/EventText");
		DialoguesToGoPanel = GameObject.Find("DialogueCanvas/DialoguesToGoPanel");
	}
}
