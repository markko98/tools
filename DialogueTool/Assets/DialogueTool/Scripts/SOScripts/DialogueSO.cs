﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Dialogue", menuName = "Dialogue/New Dialogue")]
public class DialogueSO : ScriptableObject
{
    public string dialogueName;
    public int dialogueID;
    public int currNode;
    public bool isDialogueShown;
    public List<Node> nodes = new List<Node>();
    public List<Node> nodesWhenDialogueIsDone = new List<Node>();
    public List<DialogueSO> dialoguesToGo = new List<DialogueSO>();
}

[System.Serializable]
public class Node
{
    public CharacterSO character;
    public string sentence;
    public List<string> events = new List<string>();
    public List<string> choices = new List<string>();
    public AudioClip audioClip;
    public string stringToPrint;
}