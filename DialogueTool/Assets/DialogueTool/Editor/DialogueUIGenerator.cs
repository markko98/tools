﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;
using System;

public class DialogueUIGenerator 
{
    [MenuItem(itemName: "Tools/Generate dialogue UI")]
    public static void Generate()
    {
        var filePath = Application.dataPath + "/DialogueTool/Scripts/UIGenerated.cs";
        var dialogueCanvas = GameObject.FindGameObjectWithTag(Tags.DialogueCanvas);
        var content = new List<string>();
        content.Add("using UnityEngine;");
        content.Add("public class UIGenerated : MonoBehaviour");
        content.Add("{");

        AddUIData(dialogueCanvas.gameObject, content);

        content.Add("");
        content.Add("\tpublic static void AddReferences()");
        content.Add("\t{");
        AddUIReferences(dialogueCanvas.gameObject, content, "");
        content.Add("\t}");
        content.Add("}");

        File.WriteAllLines(filePath, content);

        AssetDatabase.Refresh();
    }

    public static void AddUIData(GameObject go, List<string> content)
    {
        content.Add("\tpublic static GameObject " + go.name + ";");

        for (int i = 0; i < go.transform.childCount; i++)
        {
            AddUIData(go.transform.GetChild(i).gameObject, content);
        }
    }
    private static void AddUIReferences(GameObject go, List<string> content, string prefix)
    {
        content.Add("\t\t" + go.name + " = GameObject.Find(\"" + prefix + go.name + "\");");

        for (int i = 0; i < go.transform.childCount; i++)
        {
            var newPrefix = prefix;
            if (newPrefix == "")
            {
                newPrefix = go.name + "/";
            }
            else
            {
                newPrefix += go.name + "/";
            }
            AddUIReferences(go.transform.GetChild(i).gameObject, content, newPrefix);
        }
    }

}
