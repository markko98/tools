﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public class DialogueCreatorWindow : EditorWindow
{
    Rect headerSection;
    Rect bodySection;

    string path ="";
    string fileName = "";

    string[] pathsForSave = new string[] {"Assets_Resources_ScriptableObjects_Dialogues_",
                                          "Assets_DialogueTool_ScriptableObjects_Dialogues_"
                                          };


    string pathToSave = "";
    int choiceIndex = 0;

    bool showWarning = false;

    public static DialogueCreatorWindow dialogueCreatorWindow;

    [MenuItem("Tools/Create Dialogue")]
    static void OpenWindow()
    {
        dialogueCreatorWindow = (DialogueCreatorWindow)GetWindow(typeof(DialogueCreatorWindow));
        dialogueCreatorWindow.minSize = new Vector2(150, 250);
        dialogueCreatorWindow.maxSize = new Vector2(300, 500);
        dialogueCreatorWindow.maximized = true;
        dialogueCreatorWindow.Show();
    }

    public static void CloseWindow()
    {
        dialogueCreatorWindow.Close();
    }
    private void OnEnable()
    {
        path = "Assets/DialogueTool/Resources/";
    }
    void OnGUI()
    {
        DrawLayouts();
        DrawHeader();
        DrawBody();

    }

    void DrawLayouts()
    {
        headerSection.x = Screen.width / 3;
        headerSection.y = 5;
        headerSection.width = Screen.width;
        headerSection.height = 50f;

        bodySection.x = 5;
        bodySection.y = 25;
        bodySection.width = Screen.width;
        bodySection.height = Screen.height - 25;
    }
    void DrawHeader()
    {
        GUILayout.BeginArea(headerSection);

        GUILayout.Label("Dialogue Creator");

        GUILayout.EndArea();
    }
    void DrawBody()
    {
        GUILayout.BeginArea(bodySection);

        GUILayout.Label("Name of the text file: ");
        EditorGUI.BeginChangeCheck();
        fileName = EditorGUILayout.TextField(fileName);
        if (EditorGUI.EndChangeCheck())
        {
            if (!string.IsNullOrEmpty(fileName.ToString()))
            {
                var tempFilePath = path;
                tempFilePath += fileName + ".txt";
                if (File.Exists(tempFilePath))
                {
                    showWarning = false;
                }
                else
                {
                    showWarning = true;
                }
            }
            else
            {
                showWarning = false;
            }
        }
        GUILayout.Space(20);

        GUILayout.Label("Where to save a file: ");
        choiceIndex = EditorGUILayout.Popup(choiceIndex, pathsForSave);
        pathToSave = pathsForSave[choiceIndex];
        GUILayout.Space(20);


        if (GUILayout.Button("Create Dialogue", GUILayout.Height(40)))
        {
            var filePath = path;
            filePath += fileName + ".txt";
            if (File.Exists(filePath))
            {
                pathToSave = pathToSave.Replace("_", "/");
                pathToSave += fileName + ".asset";
                CreateDialogue(fileName, path, pathToSave);
                dialogueCreatorWindow.Close();
            }
            else
            {
                showWarning = true;
            }
        }

        if (showWarning)
        {
            EditorGUILayout.HelpBox("The text file of name " + fileName + " doesn't exist in " + path + ".", MessageType.Warning);
        }
        GUILayout.EndArea();
    }

    /// <summary>
    /// Creating a SO of type DialogueSO, parsing a text file and filling the data in SO with corresponding string from text file
    /// </summary>
    /// <param name="path">Path to text file to read</param>
    /// <param name="pathToSave">Path for saving instance of the DialogueSO</param>
    private void CreateDialogue(string textFileName, string path, string pathToSave)
    {
        DialogueSO dialogue = (DialogueSO)ScriptableObject.CreateInstance(typeof(DialogueSO));
        AssetDatabase.CreateAsset(dialogue, pathToSave);

        var textFileLines = File.ReadAllLines(path + textFileName + ".txt");

        //TODO: parse text and fill info in dialogueSO
        dialogue.dialogueName = dialogue.name;

        ParseText(textFileLines, dialogue);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    void ParseText(string[] text, DialogueSO dialogue)
    {
        var node = new Node();
        CharacterSO character;
        bool isEndDialogue = false;
        foreach (var line in text)
        {
            int indexOfPrefix = 0;
            if (line.Contains(ParsingPrefix.ID))
            {
                if (int.TryParse(line.Remove(0, ParsingPrefix.ID.Length), out int id))
                {
                    dialogue.dialogueID = id;
                }
                continue;
            }
            else if (line.Contains(ParsingPrefix.Node))
            {
                node = new Node();
                isEndDialogue = false;
                continue;
            }
            else if (line.Contains(ParsingPrefix.Name))
            {
                indexOfPrefix = line.IndexOf(ParsingPrefix.Content) + ParsingPrefix.Content.Length;
                character = Resources.Load<CharacterSO>("ScriptableObjects/Characters/" + line.Substring(indexOfPrefix));
                node.character = character;
                continue;
            }
            else if (line.Contains(ParsingPrefix.Content))
            {
                indexOfPrefix = line.IndexOf(ParsingPrefix.Content) + ParsingPrefix.Content.Length;
                node.sentence = line.Substring(indexOfPrefix);
                continue;
            }
            else if (line.Contains(ParsingPrefix.Event))
            {
                indexOfPrefix = line.IndexOf(ParsingPrefix.Event) + ParsingPrefix.Event.Length;
                node.events.Add(line.Substring(indexOfPrefix));
                continue;
            }
            else if (line.Contains(ParsingPrefix.Choice))
            {
                indexOfPrefix = line.IndexOf(ParsingPrefix.Choice) + ParsingPrefix.Choice.Length;
                node.choices.Add(line.Substring(indexOfPrefix));
                continue;
            }
            else if (line.Contains(ParsingPrefix.Print))
            {
                indexOfPrefix = line.IndexOf(ParsingPrefix.Print) + ParsingPrefix.Print.Length;
                node.stringToPrint = line.Substring(indexOfPrefix);
                continue;
            }
            else if (line.Contains(ParsingPrefix.Dialogues))
            {
                dialogue.dialoguesToGo = new List<DialogueSO>();
                continue;
            }
            else if (line.Contains(ParsingPrefix.Dialogue))
            {
                dialogue.dialoguesToGo.Add(CreateInstance<ScriptableObject>() as DialogueSO);
                continue;
            }
            else if (line.Contains(ParsingPrefix.NodeEnd.ToString()))
            {
                if (isEndDialogue)
                {
                    dialogue.nodesWhenDialogueIsDone.Add(node);
                }
                else
                {
                    dialogue.nodes.Add(node);
                }
                continue;
            }
            else if (line.Contains(ParsingPrefix.NodeWhenDone))
            {
                node = new Node();
                isEndDialogue = true;
                continue;
            }
            else if (line.Contains(ParsingPrefix.NodeWhenDone))
            {
                node = new Node();
                isEndDialogue = true;
                continue;
            }
            else if (line.Contains(ParsingPrefix.ConversationEnd.ToString()))
            {
                Debug.Log("End of conversation");
            }
            else
            {
                continue;
            }
        }
    }
}