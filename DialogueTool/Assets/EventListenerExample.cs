﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class EventListenerExample : MonoBehaviour
{
    public UnityAction listener;
    public Text txt;
    private void Start()
    {
        txt = gameObject.GetComponent<Text>();
        listener = new UnityAction(FindScholar);
        EventManager.StartListening("FindScholar", listener);
    }

    void FindScholar()
    {
        txt.color = Color.red;
        EventManager.StopListening("FindScholar", listener);
    }

}
